clean:
    @rm -rf reports/pdepend
    @rm -rf reports/logs
    @mkdir -p reports/logs
    @mkdir -p reports/pdepend
