<?php

class Router
{

    static public function parse($url, $request)
    {
        $url = trim($url);

        $indice = intval(getenv('indice'));

        $explode_url = explode('/', $url);
        $explode_url = array_slice($explode_url, $indice);
        $request->controller = $explode_url[0];
        $request->action = $explode_url[1];
        $request->params = array_slice($explode_url, 2);


    }
}
?>