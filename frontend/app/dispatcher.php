<?php

class Dispatcher
{

    private $request;

    public function dispatch()
    {
        $this->request = new Request();
        Router::parse($this->request->url, $this->request);

       $controller = $this->loadController();

        call_user_func_array([$controller, $this->request->action], $this->request->params);
    }

    public function loadController()
    {
        $name = $this->request->controller;
        $file = $name . '.php';
        require($file);
        $controller = new $name();
        return $controller;
    }
}

    // $url = $_SERVER["REQUEST_URI"];

    // $explode_url = explode('/', $url);
    // $explode_url = array_slice($explode_url, 3);


    // $controllerName = $explode_url[0];
    // $action = $explode_url[1];
    // $params = array_slice($explode_url, 2);


    // $file = $controllerName . '.php';
    // require($file);
    // if(!isset($controller)){
    //     $controller = new $controllerName();
    // }
    // call_user_func_array([$controller,$action], $params);

     
?>