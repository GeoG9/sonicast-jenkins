<?php 
include "./app/api.php";
use PHPUnit\Framework\TestCase;

final class apiTest extends TestCase
{


    public function testGetLatlongFromCity(){
        $api = new Api(true);


        $URLCities = "http://82.165.100.140:8001/weatherforecast";

        $chCities = curl_init();
        curl_setopt($chCities, CURLOPT_URL,$URLCities);
        curl_setopt($chCities, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($chCities, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($chCities, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        $resultCity = curl_exec($chCities);
        $status_codeCity = curl_getinfo($chCities, CURLINFO_HTTP_CODE); //get status code
        curl_close ($chCities);

        $api->cityList = json_decode($resultCity);


        foreach ($api->cityList as $key => $value) {
            $value->display = $value->cityName." (".$value->country.")";
        }

        $city =  $api->GetLatlongFromCity("Brest (France)");

        $this->assertEquals("Brest", $city->cityName);
        $this->assertEquals("48.3904", $city->latitude);
        $this->assertEquals("-4.495", $city->longitude);
        $this->assertEquals("France", $city->country);
    }

    public function testgetPrevisionDateNow(){
        $api = new Api(true);

        $datee = date("Y-m-d");

        $getDateTimeExport = $api->getPrevisionDate($datee);

        $dateExpected = date_format (new DateTime ('now'),'Y-m-d')."T".date('H').":00:00"."Z";

        $this->assertEquals( $dateExpected, $getDateTimeExport);


    }
    public function testgetPrevisionDateFuturDate(){
        $api = new Api(true);

        $datee = new DateTime ('now');
        $datee->modify('+2 day');
        $datee = date(date_format ($datee,'Y-m-d'));
        
        $getDateTimeExport = $api->getPrevisionDate($datee);

        $dateExpected = $datee."T00:00:00Z";

        $this->assertEquals( $dateExpected, $getDateTimeExport);

    }
    public function testgetPrevisionDatePast(){
        $api = new Api(true);

        $datee = new DateTime ('now');
        $datee->modify('-4 days');

        $datee = date_format($datee, 'Y-m-d');

        $getDateTimeExport = $api->getPrevisionDate($datee);

        $this->assertEquals( null, $getDateTimeExport);
    }

}
?>